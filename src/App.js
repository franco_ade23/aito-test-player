import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'
import SingersListPage from './pages/SingersListPage'
import SongsListPage from './pages/SongsListPage'
import NotFoundPage from './pages/NotFoundPage'

import 'bootstrap/dist/css/bootstrap.min.css'

/**
 * 
 * @description Initialize project and make routes, default route app is "/singers"
 * 
 */
const App = () => {
    return (
        <BrowserRouter>
            <div className="App">
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/singers" />
                    </Route>
                    <Route exact path="/singers" component={SingersListPage} />
                    <Route exact path="/singers/:idSong" component={SongsListPage} />
                    <Route component={NotFoundPage} />
                </Switch>
            </div>
        </BrowserRouter>
    );
}

export default App;
