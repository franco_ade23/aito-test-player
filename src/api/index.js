import { BASE_URL } from '../constants/api.constants'
import axios from 'axios'

/**
 * Create methods based on API REST.
 * The project needs only get method
 */
class Api {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
    }

    get(url) {
        return axios.get(`${this.baseUrl}${url}`)
    }
}

export default new Api(BASE_URL)