import React, {Fragment} from 'react';
import Header from '../Header/'
import SingerItem from '../SingerItem/'


/**
 *  @description Staless Component, this method render when http request is succesful.
 * //Render header with custom title and list singers provided Spotify API.
 */
const ContentSingersList = ({data}) => {
    return (
        <Fragment>
            <Header title="Lista de Artistas" />
            <section className="section">
                <div className="wrapper">
                    <div className="box">
                        {
                            data.items.map( singer => <SingerItem key={singer.id} singer={singer}/> )
                        }
                    </div>
                </div>
            </section>
        </Fragment>
    );
}

export default ContentSingersList;
