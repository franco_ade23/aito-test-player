import React from 'react';
import Header from '../Header';
import SongItem from '../SongItem'
import './index.scss'

/**
 * @description Staless Component, this render list songs
 */
const ContentSongsList = ({data, singer, search, searchSong, playSong}) => {
    return (
        <div className="ContentSongsList">
            {/* <Header title={singer.name} isImage={singer.image} /> */}
            <Header title={singer.name} />
            <section className="section">
                <div className="wrapper">
                    <div className="ContentSongsList__search">
                        <input className="search" name="search" type="text" value={search} placeholder="Buscar canción" onChange={(e)=>{ searchSong(e.target.value)}} />
                    </div>
                    <p className="ContentSongsList__lista">Lista de Canciones: </p>
                    <div className="box">
                        {
                            data.length > 0 ? data.map( song => <SongItem key={song.id} song={song} playSong={playSong}/> ) : <span>No hay coincidencias</span>
                        }
                    </div>
                </div>
            </section>
        </div>
    );
}

export default ContentSongsList;
