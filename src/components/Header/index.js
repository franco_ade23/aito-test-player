import React from 'react';
import PropTypes from 'prop-types';
import './index.scss'


/**
 * @description Staless component, this components has custom title, catch props with destructuring object JS
 * @param {string} title
 * @param {string} isImage
 */
const Header = ({title, isImage}) => {
    return (
        <header className="Header">
            <div className="container">
                <h2 className="Header__title">{title}</h2>
                {
                    isImage && <img className="Header__image" src={isImage} alt="test"/>
                }
            </div>  
        </header>
    );
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    isImage: PropTypes.string
}

Header.defaultProps = {
    title: "Title Page",
}

export default Header;