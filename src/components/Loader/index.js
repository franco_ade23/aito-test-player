import React from 'react';
import './index.scss'

/**
 * @description Staless component, this render when promise axios is on pending status
 */
const Loader = () => {
    return (
        <div className="Loader">
            <div className="Loader__ripple">
                <div></div>
                <div></div>
            </div>
        </div>
    );
}

export default Loader;
