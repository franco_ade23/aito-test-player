import React, { Component } from 'react'
import PlayIcon from '../../images/play.png'
import PauseIcon from '../../images/pause.png'
import './index.scss'

/**
 * @description Useful component, this component manages logic on play/pause action
 * this component gets selected song like props
 * 
 */
class Player extends Component {

    state = {
        isPlay: true
    }

    /**
     * @description Lifecycle method
     */
    componentDidMount() {
        const {song} = this.props
        if(song){
            this.initPlay()
        }
    }
    
    /**
     * @description Lifecycle method, this method runs when component update props
     */
    componentDidUpdate(prevProps) {

        // If props is diferen and prevProps means user changed song
        if (this.props.song.id !== prevProps.song.id) {
            if(this.customPlayer){
                this.customPlayer.pause()
                this.initPlay()
            }        
        }
    }
    

    /**
     * @description Initalize player music when song is selected(click)
     */
    initPlay = () => {
        const {song} = this.props
        if(song.preview_url){
            this.customPlayer = new Audio(song.preview_url)
            this.customPlayer.play()
            this.setState({
                isPlay: true
            })
        }
    }

    /**
     * @description toggle logic pause and play song
     */
    togglePlay = () => {
        if(this.customPlayer.paused){
            this.setState({
                isPlay: true
            }, ()=> {
                this.customPlayer.play()
            })
        }else{
            this.setState({
                isPlay: false
            }, ()=> {
                this.customPlayer.pause()
            })
        }
    }


    /**
     * @description Format artist's name
     * @return {String} Returns artist's name
     * @example 
     * // returns Shakira, Camilo 
     * getSingers([Shakira, Camilo])
     */
    getSingers = (artists=[]) => {
        let text = ""
        artists.forEach((artist, index )=>{
            if( index === artists.length - 1){
                text += artist.name
            }else{
                text += `${artist.name}, `
            }
        })
        return text
    }

    render() {
        const {song} = this.props
        const {isPlay} = this.state
        return (
            <div className="Player">
                <div className="Player__container">
                    {
                        isPlay ? 
                            <img onClick={this.togglePlay} className="Player__icon" src={PauseIcon} alt="test" /> :
                            <img onClick={this.togglePlay} className="Player__icon" src={PlayIcon} alt="test" />
                    }
                    <div>
                        <div>{song.name}</div>
                        <div>{this.getSingers(song.artists)}</div>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * This methos runs when component is unmounted
     */
    componentWillUnmount() {
        if(this.customPlayer){
            this.customPlayer.pause()
        }
    }
    
}


export default Player;