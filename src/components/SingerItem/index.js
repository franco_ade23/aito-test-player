import React from 'react';
import { withRouter } from 'react-router-dom'
import './index.scss'

/**
 * @description StalessComponent, change route '/singers/:idSinger' when this components is clicked
 */
const SingerItem = ( {singer, history} ) => {
    return (
        <article 
            className="SingerItem" 
            onClick={()=>{history.push({ 
                pathname: `/singers/${singer.artists[0].id}`, 
                state:{
                    info: true,
                    name: singer.artists[0].name,
                    image: singer.images[0].url
                }
            })}}>
            <div>
                <img className="SingerItem__picture" src={singer.images[0].url} alt="test"></img>
            </div>
            <div className="SingerItem__name">
                {singer.artists[0].name}
            </div>
            <div className="ml-4 font-italic">
                <span className="SingerItem__albumText">Album: </span>
                <span className="SingerItem__albumName">
                    {singer.name}
                </span>
            </div>
        </article>
    );
}



export default withRouter(SingerItem);
