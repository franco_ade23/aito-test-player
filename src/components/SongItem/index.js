import React from 'react';
import './index.scss'

/**
 * @description Format artist's name
 * @return {String} Returns artist's name
 * @example 
 * // returns Shakira, Camilo 
 * getSingers([Shakira, Camilo])
 */
const getSingers = (artists=[]) => {
    let text = ""
    artists.forEach((artist, index )=>{
        if( index === artists.length - 1){
            text += artist.name
        }else{
            text += `${artist.name}, `
        }
    })
    return text
}


/**
 * @description Format song's time on milliseconds
 */
const getTime = (duration = 1000) => {
    return duration
}


/**
 * @description StalessComponent, onClick in this component play current song
 */
const SongItem = ({song, playSong}) => {
    return (
        <article className="SongItem" onClick={()=>{playSong(song)}}>
            <div className="SongItem__name">
                {song.name}
            </div>
            <div className="SongItem__Info">
                <span className="SongItem__singers">{getSingers(song.artists)}</span>
                <span className="SongItem__time">{getTime(song.duration_ms)}</span>
            </div>
        </article>
    );
}

export default SongItem;
