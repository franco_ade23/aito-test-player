import React from 'react';

/**
 * @description component(Page) render when route doesn't match with defined routes
 */

const NotFoundPage = () => {
    return (
        <div>
            <h2>Página no encontrada </h2>
        </div>
    );
}

export default NotFoundPage;
