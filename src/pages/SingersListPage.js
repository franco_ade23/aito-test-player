import React, { Component } from 'react'
import Content from '../components/ContentSingersList/'
import Loader from '../components/Loader/'
import Api from '../api/index'

/**
 * 
 * Stateful component.
 * This component(Page) render when route's path matches with '/singers'
 * 
 */
class SingersListPage extends Component {

    state = {   
        data: null,
        loading: true,
        error: false
    }

    /**
     * This method runs after component has been rendered, ngOnInit on Angular 
     * Make http service and save data on state
     */
    async componentDidMount() {
        try{
            const url = 'browse/new-releases'
            const results = await Api.get(url)
            this.setState({
                loading: false,
                data: results.data.albums
            })
        }catch{
            this.setState({
                error: true
            })
        }
    }
    
    /**
     * This method render HTML
     */
    render() {
        const { data, loading, error } = this.state
        if( loading ) return <Loader />
        if( error ) return <p>Error</p>
        return (
            <Content data={data}/>
        )
    }
}

export default SingersListPage
