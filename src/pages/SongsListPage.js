import React, { Component, Fragment } from 'react'
import { Redirect } from 'react-router-dom'
import Content from '../components/ContentSongsList'
import Loader from '../components/Loader'
import Player from '../components/Player'
import Api from '../api/index'

/**
 * 
 * @description Stateful component, component(Page) render when route's path matches with '/singers/*idSinger'
 * 
 */
class SongsListPage extends Component {

    state = { 
        data: null,
        dataOrigi: null,
        loading: true,
        error: false,
        search: "",
        songSelected: null
    }

    /**
     * @description Lifecycle method runs after component has been rendered, ngOnInit on Angular 
     * // Make http service and save songs on state
     */
    async componentDidMount() {
        const {match} = this.props
        try{        
            // Catch idSong from route's params
            const idSong = match.params.idSong
            const url = `artists/${idSong}/top-tracks?country=PE`
            const results = await Api.get(url)

            // Save tracks on data and dataOrigi.
            // dataOrigi: save similar info but it's used for filters
            this.setState({
                loading: false,
                data: results.data.tracks,
                dataOrigi: results.data.tracks
            })
        }catch{
            this.setState({
                error: true
            })
        }
    }
    
    /**
     * @description Method gets input's value when this change, update state and filter results to show
     */
    searchSong = search => {
        this.setState({
            search
        }, ()=>{
            if(search.length === 0){
                this.setState({
                    data: this.state.dataOrigi
                })
            }else{
                const data = this.state.dataOrigi.filter( song => song.name.toLowerCase().includes(search.toLowerCase()))
                this.setState({
                    data
                })
            }
        })
    }


    /**
     * @description This method runs when song is selected to play, 
     * //if song's data doesn't have preview_url save null on state and don't show player
     */
    playSong = songSelected => {
       if(songSelected.preview_url){
            this.setState({
                songSelected
            })
       }else{
            this.setState({
                songSelected: null
            })
       } 
    }

    render() {
        const { data, loading, error, search, songSelected } = this.state
        const {location} = this.props
        if (!location.state) return <Redirect to="/singers"/>
        if( loading ) return <Loader />
        if( error ) return <p>Error</p>
        return (
            <Fragment>
                <Content
                    data={data} 
                    singer={location.state} 
                    search={search}
                    searchSong={this.searchSong}
                    playSong={this.playSong}
                />
                {
                    songSelected && <Player song={songSelected} />
                }
            </Fragment>
            
        )
    }
}

export default SongsListPage
